import { ComponentFixture, TestBed } from '@angular/core/testing';
import { TestComponent } from './test.component';
import { Component, DebugElement } from '@angular/core';
import { By } from '@angular/platform-browser';

describe('TestComponent', () => {
  let fixture: ComponentFixture<DummyComponent>;
  let component: DummyComponent;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        DummyComponent,
      ],
    }).compileComponents();

    fixture = TestBed.createComponent(DummyComponent);
    component = fixture.componentInstance;
    jest.spyOn(component, 'buttonClicked');
  });


  describe('when no title is passed ', () => {
    it('should display empty title', () => {
      expect(getTitle().nativeElement.innerHTML).toBe('');
    });

    describe('and user clicks button', () => {
      beforeEach(() => {
        getButton().nativeElement.click();
        fixture.detectChanges();
      });

      it('should emit empty string', () => {
        expect(component.buttonClicked).toHaveBeenCalledTimes(1);
        expect(component.buttonClicked).toHaveBeenCalledWith('');
      });
    });
  });

  describe('when "my title" value is passed as "title"', () => {
    beforeEach(() => {
      component.title = 'my title';
      fixture.detectChanges();
    });

    it('should display "my title"', () => {
      expect(getTitle().nativeElement.innerHTML).toBe('my title');
    });

    describe('and user clicks button', () => {
      beforeEach(() => {
        getButton().nativeElement.click();
        fixture.detectChanges();
      });

      it('should emit empty string', () => {
        expect(component.buttonClicked).toHaveBeenCalledTimes(1);
        expect(component.buttonClicked).toHaveBeenCalledWith('my title');
      });
    });
  });

  const getTitle = (): DebugElement => {
    return fixture.debugElement.query(By.css('.title'));
  }

  const getButton = (): DebugElement => {
    return fixture.debugElement.query(By.css('.button'));
  }
});

@Component({
  selector: 'app-dummy',
  template: `
    <app-test [title]="title" (buttonClick)="buttonClicked($event)"></app-test>`,
  styleUrls: ['./test.component.css']
})
export class DummyComponent {
  title: string = '';

  buttonClicked(value: string): void {

  }
}
