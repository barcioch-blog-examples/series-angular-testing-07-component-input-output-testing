import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent {
  @Input() title = '';
  @Output() buttonClick = new EventEmitter<string>();

  click(): void {
    this.buttonClick.emit(this.title);
  }
}
